package it.polimi.ingsw.tdd.model;


public class Bitcoin extends Currency
{

	@Override
	protected float toDefaultCurrency()
	{
		return 246.0185f;
	}

}
