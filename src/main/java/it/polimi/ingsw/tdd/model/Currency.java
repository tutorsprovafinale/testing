package it.polimi.ingsw.tdd.model;

import it.polimi.ingsw.tdd.exceptions.InvalidAmountException;

public abstract class Currency
{	
	private float amount;

	protected abstract float toDefaultCurrency(); 
	
	public static void exchange(Currency from, Currency to) throws InvalidAmountException {
		if(from.getAmount()<0)
			throw new InvalidAmountException();
		
		to.setAmount((from.toDefaultCurrency()/to.toDefaultCurrency())*from.getAmount());
	}
	
	public void setAmount(float amount){
		this.amount = amount;
	}
	
	public float getAmount(){
		return amount;
	}
	

}
