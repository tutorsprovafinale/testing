package it.polimi.ingsw.tdd.model;


public class Dollar extends Currency
{
	@Override
	protected float toDefaultCurrency()
	{
		return 1.0f;
	}

}
