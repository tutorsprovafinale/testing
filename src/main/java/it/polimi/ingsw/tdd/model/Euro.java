package it.polimi.ingsw.tdd.model;


public class Euro extends Currency
{

	@Override
	protected float toDefaultCurrency()
	{
		return 1.1226f;
	}

}
