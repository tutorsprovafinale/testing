package it.polimi.ingsw.tdd.model.test;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.tdd.exceptions.InvalidAmountException;
import it.polimi.ingsw.tdd.model.Bitcoin;
import it.polimi.ingsw.tdd.model.Currency;
import it.polimi.ingsw.tdd.model.Dollar;
import it.polimi.ingsw.tdd.model.Euro;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestExchange
{
	private final float ERROR_LIMIT = 0.01f;
	
	
	@Test
	public void tautology() {
		assertTrue(true);
	}
	
	@Test
	public void testEuroDollar() throws InvalidAmountException 
	{	
		Euro e = new Euro();
		e.setAmount(3);
		
		Dollar d = new Dollar();
		
		Currency.exchange(e, d);
				
		assertTrue(Math.abs(d.getAmount() - 3.36) < ERROR_LIMIT);
	}
	
	@Test
	public void testEuroBitcoin() throws InvalidAmountException 
	{	
		Euro e = new Euro();
		e.setAmount(5);
		
		Bitcoin b = new Bitcoin();
		Currency.exchange(e, b);
		
		assertTrue(Math.abs(b.getAmount() - 0.02) < ERROR_LIMIT);
	}
	
	@Test
	public void testSourceImmutability() throws InvalidAmountException 
	{	
		float initialAmount = 20.0f;
		
		Bitcoin b = new Bitcoin();
		b.setAmount(20);
				
		Currency.exchange(b, new Dollar());
				
		assertTrue(b.getAmount() == initialAmount);
	}
	
	@Test
	public void testAmountZero() throws InvalidAmountException 
	{	
		Euro e = new Euro();
		e.setAmount(0);
		
		Bitcoin b = new Bitcoin();
		b.setAmount(1);
		
		Currency.exchange(e, b);
		
		assertTrue(b.getAmount() == 0);
	}
	
	@Test
	public void testReversibility() throws InvalidAmountException 
	{	
		float initialAmount = 25.3f;
		
		Euro e = new Euro();
		e.setAmount(initialAmount);
		
		Bitcoin b = new Bitcoin();
		
		Currency.exchange(e, b);
		Currency.exchange(b, e);

		assertTrue(Math.abs(e.getAmount() - initialAmount) < ERROR_LIMIT);
	}
	
	@Test(expected=InvalidAmountException.class)
	public void testInvalidAmount() throws InvalidAmountException 
	{	
		Euro e = new Euro();
		e.setAmount(-5);
		
		Currency.exchange(e, new Bitcoin());
	}
	
	@BeforeClass
	public static void begin(){
		//System.out.println("Begin");
	}
	
	@Before
	public void prepare(){
		//System.out.println("Prepare");
	}
	
	@After
	public void finalize(){
		//System.out.println("Finalize");
	}
	
	@AfterClass
	public static void end(){
		//System.out.println("End");
	}
	
}
